#!/bin/sh

if [ "--help" = "$1" ]; then
    echo "      youtube video-streamer
    
      Usage: yt.sh <search terms> [-v] 
      
      -v                     enable video"
else 
    url="$(youtube-dl -g -f webm ytsearch:"$1")";
    if [ "-v" = "$2" ]; then
        mpv ${url};
    else
        mpv "--no-video" ${url};
    fi
fi

#-----------------------
#               FOR V2:
#This is how to do searches with mpv directly
#mpv --no-video "ytdl://ytsearch:search term";